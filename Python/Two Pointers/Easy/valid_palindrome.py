# A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters, it reads the same forward and backward. Alphanumeric characters include letters and numbers.

# Given a string s, return true if it is a palindrome, or false otherwise.

# Example 1:
# Input: s = "A man, a plan, a canal: Panama"
# Output: true
# Explanation: "amanaplanacanalpanama" is a palindrome.

# Example 2:
# Input: s = "race a car"
# Output: false
# Explanation: "raceacar" is not a palindrome.

# Example 3:
# Input: s = " "
# Output: true
# Explanation: s is an empty string "" after removing non-alphanumeric characters.
# Since an empty string reads the same forward and backward, it is a palindrome.

class Solution:
    def isPalindrome(self, s: str) -> bool:
        result = ''
        for i in s.upper():
            if i.isalnum():
                result += i
        return result == result[::-1]
    
# time complexity: O(n)
# space complexity: O(n)
# extra memory is used for the result variable and used to reverse the string

class Solution(object):
    def alphaNum(self, c):
        return (ord('A') <= ord(c) <= ord('Z')) or (ord('a') <= ord(c) <= ord('z')) or (ord('0') <= ord(c) <= ord('9'))

    def isPalindrome(self, s):
        l = 0
        r = len(s)-1

        while l < r:
            # we want to do l < r to make sure it doesn't go out of bounds
            while l < r and not self.alphaNum(s[l]):
                l += 1
            while l < r and not self.alphaNum(s[r]):
                r -= 1
            if s[l].lower() != s[r].lower():
                print(s[l], s[r])
                return False

            l = l + 1
            r = r - 1

        return True

# time complexity: O(n)
# space complexity: O(1)