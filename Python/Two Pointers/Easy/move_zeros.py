# Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.

# Note that you must do this in-place without making a copy of the array.

# Example 1:
# Input: nums = [0,1,0,3,12]
# Output: [1,3,12,0,0]
# Example 2:

# Input: nums = [0]
# Output: [0]

class Solution:
    def moveZeroes(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        # use two pointer solution
        # second pointer will be number that's greater than 0
        # first pointer will be the place it needs to swap (0)
        # if i = 0, stay
        # if i and j > 0, increment

        i = 0
        j = 1

        while j < len(nums):
            if nums[j] != 0 and nums[i] == 0:
                nums[i],nums[j] = nums[j],nums[i]
                i+=1
                j+=1
            elif (nums[j] == 0 and nums[i] != 0) or (nums[j] != 0 and nums[i] != 0):
                i+= 1
                j+=1
            else:
                j += 1
        return nums
    
# top solution
class Solution:
    def moveZeroes(self, nums: list) -> None:
        slow = 0
        for fast in range(len(nums)):
            if nums[fast] != 0 and nums[slow] == 0:
                nums[slow], nums[fast] = nums[fast], nums[slow]

            # wait while we find a non-zero element to
            # swap with you
            if nums[slow] != 0:
                slow += 1