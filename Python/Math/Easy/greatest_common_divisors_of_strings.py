# For two strings s and t, we say "t divides s" if and only if s = t + ... + t (i.e., t is concatenated with itself one or more times).

# Given two strings str1 and str2, return the largest string x such that x divides both str1 and str2.

# Example 1:
# Input: str1 = "ABCABC", str2 = "ABC"
# Output: "ABC"

# Example 2:
# Input: str1 = "ABABAB", str2 = "ABAB"
# Output: "AB"

# Example 3:
# Input: str1 = "LEET", str2 = "CODE"
# Output: ""

class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        # the divisor for both string would need to have a mod of 0 when we divide by length of either string
        # look at the shorter string out of the two to find the common divisor
            # since we want the largest divisor, we can start by checking the full length of short_str
                # check if length of short_str % long_str == 0
                # if yes, check how many multiples of the shorter_str would create the longer_str and compare
                # if no, check a substring that has a length that is % long_str == 0

        len1, len2 = len(str1), len(str2)

        if len1 > len2:
            short_str = str2
            long_str = str1
        else:
            short_str = str1
            long_str = str2

        for i in range(len(short_str)-1, -1 , -1):
            divisor = short_str[:i+1]
            if len(long_str) % len(divisor) == 0 and len(short_str) % len(divisor) == 0:
                long_multiplier = len(long_str)//len(divisor)
                short_multiplier = len(short_str)//len(divisor)
                if divisor * (long_multiplier) == long_str and divisor * (short_multiplier) == short_str:
                    return divisor
        return ""
        
class Solution:
    def gcdOfStrings(self, str1: str, str2: str) -> str:
        len1, len2 = len(str1), len(str2)

        def isDivisor():
            if len1 % l or len2 % l:
                return False
            f1, f2 = len1 // l, len2// l
            return str[:l] * f1 == str1 and str[:l] * f2 == str2

        for l in range(min(len1, len2), 0, -1):
            if isDivisor(l):
                return str[:l]
            
        return ""
    
# reviewed 9/19/23