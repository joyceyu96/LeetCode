# Given an array of integers temperatures represents the daily temperatures, return an array answer such that answer[i] is the number of days you have to wait after the ith day to get a warmer temperature. If there is no future day for which this is possible, keep answer[i] == 0 instead.

 

# Example 1:

# Input: temperatures = [73,74,75,71,69,72,76,73]
# Output: [1,1,4,2,1,1,0,0]
# Example 2:

# Input: temperatures = [30,40,50,60]
# Output: [1,1,1,0]
# Example 3:

# Input: temperatures = [30,60,90]
# Output: [1,1,0]

class Solution:
    def dailyTemperatures(self, temperatures: List[int]) -> List[int]:
        # have an output list that is the same length of the temperatures list
        output = [0] * len(temperatures)

        stack = [] #list of pairs [temp, ind]

        # iterate over temperatures list
        for i,t in enumerate(temperatures):
            # compare current temp with the most recent temp
            # if curr temp > most recent temp:
                # pop from the stack
                # update the difference in indexes to the lower temp to the output list
            # otherwise just append to the list if curr_temp is less than or equal to curr_temp
                
            while stack and t > stack[-1][0]:
                stackT, stackI = stack.pop()
                output[stackI] = i - stackI
            stack.append([t,i])
        return output

# reviewed 11/28/23
# reviewed 12/6/23