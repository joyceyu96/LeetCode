# You are given an array of strings tokens that represents an arithmetic expression in a Reverse Polish Notation.

# Evaluate the expression. Return an integer that represents the value of the expression.

# Note that:

# The valid operators are '+', '-', '*', and '/'.
# Each operand may be an integer or another expression.
# The division between two integers always truncates toward zero.
# There will not be any division by zero.
# The input represents a valid arithmetic expression in a reverse polish notation.
# The answer and all the intermediate calculations can be represented in a 32-bit integer.
 

# Example 1:
# Input: tokens = ["2","1","+","3","*"]
# Output: 9
# Explanation: ((2 + 1) * 3) = 9

# Example 2:
# Input: tokens = ["4","13","5","/","+"]
# Output: 6
# Explanation: (4 + (13 / 5)) = 6

# Example 3
# Input: tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
# Output: 22
# Explanation: ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
# = ((10 * (6 / (12 * -11))) + 17) + 5
# = ((10 * (6 / -132)) + 17) + 5
# = ((10 * 0) + 17) + 5
# = (0 + 17) + 5
# = 17 + 5
# = 22

class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        stack = deque()
        for i in tokens:
            if i not in '+-*/':
                stack.appendleft(int(i))
            elif i == '+':
                total = stack[0] + stack[1]
                stack.popleft()
                stack.popleft()
                stack.appendleft(total)
            elif i == '-':
                total = stack[1] - stack[0]
                stack.popleft()
                stack.popleft()
                stack.appendleft(total)
            elif i == '*':
                total = stack[0] * stack[1]
                stack.popleft()
                stack.popleft()
                stack.appendleft(total)
            elif i == '/':
                total = stack[1]//stack[0] if stack[0]*stack[1]>0 else (stack[1]+(-stack[1]%stack[0]))//stack[0]
                stack.popleft()
                stack.popleft()
                stack.appendleft(total)

        return stack[0]
    
# neetcode solution using a list instead of a deque
class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        stack = []
        for i in tokens:
            if i == '+':
                stack.append(stack.pop() + stack.pop())
            elif i == '-':
                a,b = stack.pop(), stack.pop()
                stack.append(b-a)
            elif i == '*':
                stack.append(stack.pop() * stack.pop())
            elif i == '/':
                a,b = stack.pop(), stack.pop()
                stack.append(int(b/a))
            else:
                stack.append(int(i))
        return stack[0]
    
# reviewed 11/28/23