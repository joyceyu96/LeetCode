# You are given an integer array nums consisting of n elements, and an integer k.

# Find a contiguous subarray whose length is equal to k that has the maximum average value and return this value. Any answer with a calculation error less than 10-5 will be accepted.

 

# Example 1:

# Input: nums = [1,12,-5,-6,50,3], k = 4
# Output: 12.75000
# Explanation: Maximum average is (12 - 5 - 6 + 50) / 4 = 51 / 4 = 12.75
# Example 2:

# Input: nums = [5], k = 1
# Output: 5.00000

class Solution:
    def findMaxAverage(self, nums: List[int], k: int) -> float:
        # window length = k
        # input = list of ints, and size of window
        # we want to find the max average of all the numbers in the window

        # max_average = sum(nums[0:0+k])/k
        
        # for i in range(1,len(nums)-k+1):
        #     if nums[i+k-1] > nums[i-1]:
        #         average = sum(nums[i:i+k])/k
        #         if average > max_average:
        #             max_average = average
        # return max_average
        max_sums = sum(nums[:k])
        sums = max_sums
        for i in range(len(nums)-k):
            sums += nums[i+k] - nums[i]
            if sums > max_sums: max_sums = sums
        return (max_sums)/k
    
# reviewed 10/6/23
