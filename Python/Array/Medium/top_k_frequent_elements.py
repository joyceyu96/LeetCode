# Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.

# Example 1:
# Input: nums = [1,1,1,2,2,3], k = 2
# Output: [1,2]

# Example 2:
# Input: nums = [1], k = 1
# Output: [1]
 
# Constraints:

# 1 <= nums.length <= 105
# -104 <= nums[i] <= 104
# k is in the range [1, the number of unique elements in the array].
# It is guaranteed that the answer is unique.

# Follow up: Your algorithm's time complexity must be better than O(n log n), where n is the array's size.

class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        dict = {}
        for i in nums:
            if i not in dict:
                dict[i] = 1
            else:
                dict[i] += 1

        sorted_dict = sorted(dict.items(), key=lambda x:x[1], reverse=True )

        result = [sorted_dict[i][0] for i in range(k)]

        return result
    
# using bucket sort for O(N) time complexity
class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        count = {}
        bucket = [[] for i in range(len(nums)+1)]
        
        for i in nums:
            # adds 1 to the value at count[n], also sets count[n] to 0 if count[n] does not exist yet
            count[i] = 1 + count.get(i,0)

        for num,count in count.items():
            bucket[count].append(num)

        result = []
        for i in range(len(bucket)-1, 0, -1):
            for n in bucket[i]:
                result.append(n)
                if len(result) == k:
                    return result
                
# reviewed 11/24/23