# Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.

# You must write an algorithm that runs in O(n) time.

# Example 1:
# Input: nums = [100,4,200,1,3,2]
# Output: 4
# Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.

# Example 2:
# Input: nums = [0,3,7,2,5,8,4,6,0,1]
# Output: 9

# time complexity is O(n log n)
class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        ordered_nums = sorted(set(nums))
        longest_consec = 0
        count = 1
        if len(ordered_nums) == 1:
            return 1
        for i in range(1,len(ordered_nums)):
            if ordered_nums[i] - ordered_nums[i-1] == 1:
                count += 1
            else:
                count = 1
            if count > longest_consec:
                longest_consec = count

        return longest_consec 
    
# solution with better time complexity O(n)
class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        set_nums = set(nums)
        longest_seq = 0

        for i in nums:
            # check if its the start of a sequence 
            if i -1 not in set_nums:
                length = 1
                while i + length in set_nums:
                    length += 1
                longest_seq = max(length, longest_seq)

        return longest_seq